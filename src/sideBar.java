import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static javafx.collections.FXCollections.observableArrayList;

public class sideBar extends VBox {
    VBox sideBarPane;
    ListView optionListView;
    Button previewButt;
    Button timerButt;
    String timerStatus;

    public String getTimerStatus() {
        return timerStatus;
    }

    public void setTimerStatus(String s) {
        this.timerStatus = s;
    }

    public VBox getSideBarPane() {
        return sideBarPane;
    }

    public void setSideBarPane(VBox sideBarPane) {
        this.sideBarPane = sideBarPane;
    }

    public ListView getOptionListView() {
        return optionListView;
    }

    public void setOptionListView(ListView optionListView) {
        this.optionListView = optionListView;
    }

    public Button getPreviewButt() {
        return previewButt;
    }

    public void setPreviewButt(Button previewButt) {
        this.previewButt = previewButt;
    }

    public Button getTimerButt() {
        return timerButt;
    }

    public void setTimerButt(Button timerButt) {
        this.timerButt = timerButt;
    }

    public Label getTimeLabel() {
        return timeLabel;
    }

    public void setTimeLabel(Label timeLabel) {
        this.timeLabel = timeLabel;
    }

    public TextField getTimerTextField() {
        return timerTextField;
    }

    public void setTimerTextField(TextField timerTextField) {
        this.timerTextField = timerTextField;
    }

    Label timeLabel;
    TextField timerTextField;

    public void updatePreview(Image image) {
        previewButt.setGraphic(new ImageView(image));
    }

    public void updatePreview(String filename) {
        updatePreview(new Image(filename));
    }

    public void setTimerStart() {
        timerButt.setText("Start");
        timerStatus = "Start";
        timerButt.setStyle("-fx-base: DARKGREEN");
    }

    public void setTimerStop() {
        timerButt.setText("Stop");
        timerStatus = "Stop";
        timerButt.setStyle("-fx-base: DARKRED");
    }

    public void handleTimerClick() {
        if (timerStatus == "Stop") {
            setTimerStop();
        } else {
            setTimerStart();
        }
    }

    public sideBar(int minButtSize, int mainButtPads, int buttGapSize) {
        String[] options = {"Lego", "Numbers", "Pets", "Scenery"};

        sideBarPane = new VBox();
        previewButt = new Button();
        timerButt = new Button();
        timeLabel = new Label("Time: ");
        optionListView = new ListView();
        timerTextField = new TextField("0:00");

        timerStatus = "Start";
        setTimerStart();

        optionListView.setItems(observableArrayList(options));

        previewButt.setDisable(true);

        HBox timerPane = new HBox();

        timerPane.getChildren().addAll(timeLabel, timerTextField);

        previewButt.setMinSize(minButtSize, minButtSize);
        timerButt.setMinSize(minButtSize, 30);
        optionListView.setMinWidth(minButtSize);
        optionListView.setPrefHeight(150);

        sideBarPane.getChildren().addAll(previewButt, optionListView, timerButt, timerPane);
        sideBarPane.setMinWidth(minButtSize);

        sideBarPane.setPadding(new Insets(mainButtPads,mainButtPads,0,0));
        sideBarPane.setSpacing(buttGapSize);
        getChildren().addAll(sideBarPane);
        setMinWidth(minButtSize);
    }
}

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.event.EventHandler;


/* The mainBtnPane includes all 16 buttons that are interactable */
public class mainBtnPane extends GridPane {
    ButtonSquare[][] buttons;

    public ButtonSquare[][] getButtons() {
        return buttons;
    }

    public void setButtons(ButtonSquare[][] buttons) {
        this.buttons = buttons;
    }

    public void setButtonEvent(EventHandler ev) {
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                buttons[x][y].getBtn().setOnMouseClicked(ev);
            }
        }
    }

    public mainBtnPane(int minBtnSize, int mainBtnPadding, int btnGapSize) {
        buttons = new ButtonSquare[4][4];
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                buttons[x][y] = new ButtonSquare();
                buttons[x][y].getBtn().setMinSize(minBtnSize, minBtnSize);
                buttons[x][y].setX(x);
                buttons[x][y].setY(y);
                buttons[x][y].getBtn().setId(x + "" + y);

                add(buttons[x][y].getBtn(), x, y);
            }
        }
        setHgap(btnGapSize);
        setVgap(btnGapSize);

        setPadding(new Insets(mainBtnPadding, mainBtnPadding, mainBtnPadding, mainBtnPadding));

        // Constructor blanks out board
        logic.setBlankBoard(buttons);

        setMinSize(751, 751);
    }
}

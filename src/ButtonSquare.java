import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class ButtonSquare {
    public Button btn;
    public String fileName;
    public int    order;
    public Node   image;
    public int    x;
    public int    y;

    public Node getImage() {
        return image;
    }

    public void setImage(Node image) {
        this.image = image;
    }

    public ButtonSquare () {
        btn = new Button();
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Button getBtn() {
        return btn;
    }

    public void setBtn(Button btn) {
        this.btn = btn;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}

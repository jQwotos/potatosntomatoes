import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import static java.lang.Math.floor;

public class slidePuzzleGame extends Application {
    int minBtnSize = 187;
    int btnGapSize = 1;
    int mainBtnPadding = 10;

    int blankX = 0;
    int blankY = 0;

    int time = 0;

    String selected;

    public void start (Stage primaryStage) {
        // I wanted to try using one of the nicer panes that auto scale so we will be using
        // Gridpanes and VBoxes
        GridPane primaryGridPane = new GridPane();
        sideBar  sideBarV = new sideBar(minBtnSize, mainBtnPadding, btnGapSize);
        mainBtnPane mainBtnPaneV = new mainBtnPane(minBtnSize, mainBtnPadding, btnGapSize);

        Timeline updateTimer = new Timeline(new KeyFrame(Duration.millis(1000),
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        time += 1;
                        sideBarV.getTimerTextField().setText(String.format("%d:%02d", (int) floor(time / 60), (int) (time % 60)));
                    }
                }));
        updateTimer.setCycleCount(Timeline.INDEFINITE);

        sideBarV.getTimerButt().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (sideBarV.getTimerStatus().equals("Start")) {
                    updateTimer.play();
                    sideBarV.setTimerStop();
                    sideBarV.getTimerTextField().setText(String.format("%d:%02d", (int) floor(time / 60), (int) (time % 60)));
                } else {
                    updateTimer.stop();
                    time = 0;
                    sideBarV.setTimerStart();
                    // logic.setBlankBoard(mainBtnPaneV.getButtons());
                }
            }
        });

        primaryGridPane.add(mainBtnPaneV, 1, 1);
        primaryGridPane.add(sideBarV, 2, 1);

        sideBarV.getOptionListView().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sideBarV.setTimerStart();
                time = 0;
                updateTimer.stop();
                logic.setRandomBoard(sideBarV.getOptionListView().getSelectionModel().getSelectedItem().toString(), mainBtnPaneV.getButtons());
                logic.setPreview(sideBarV.getOptionListView().getSelectionModel().getSelectedItem().toString(), sideBarV.getPreviewButt());
                blankX = blankY = 0;
            }
        });

        mainBtnPaneV.setButtonEvent(new EventHandler() {
            @Override
            public void handle(Event event) {
                if (sideBarV.getTimerStatus().equals("Start")) {
                    sideBarV.setTimerStop();
                    updateTimer.play();
                }
                if (logic.checkIfWin(mainBtnPaneV.getButtons(), blankX, blankY)) {
                    sideBarV.setTimerStart();
                    updateTimer.stop();
                    mainBtnPaneV.getButtons()[blankX][blankY].getBtn().setGraphic(mainBtnPaneV.getButtons()[blankX][blankY].getImage());
                }
                selected = ((Button) event.getSource()).getId();
                // Neighbor checking is done by logic, this ensures that EVERY swap is valid
                if (logic.swap(mainBtnPaneV.getButtons(),mainBtnPaneV.getButtons()[blankX][blankY], mainBtnPaneV.getButtons()[Character.getNumericValue(selected.charAt(0))][Character.getNumericValue(selected.charAt(1))])) {
                    blankX = Character.getNumericValue(selected.charAt(0));
                    blankY = Character.getNumericValue(selected.charAt(1));
                }
            }
        });

        primaryStage.setTitle("Slide Puzzle Game");
        primaryStage.setScene(new Scene(primaryGridPane, 965, 768));

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

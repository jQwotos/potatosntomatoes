import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class logic {
    public static ButtonSquare swapHolder = new ButtonSquare();

    public static boolean isNeighbor(ButtonSquare one, ButtonSquare two) {
        if ((((one.getX() - 1 == two.getX()) || (one.getX() + 1 == two.getX())) && one.getY() == two.getY()) || (((one.getY() + 1 == two.getY()) || (one.getY() - 1 == two.getY())) && one.getX() == two.getX())) {
            return true;
        }
        return false;
    }

    public static boolean checkIfWin(ButtonSquare[][] btns, int blankx, int blanky) {
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                // Check if the button is not the blank button or in the wrong spot then fail the check
                if ((x != blankx && y != blanky) || btns[x][y].getX() != Character.getNumericValue(btns[x][y].getBtn().getId().charAt(0)) ||
                        btns[x][y].getY() != Character.getNumericValue(btns[x][y].getBtn().getId().charAt(1))) {
                    return false;
                }
            }
        }
        // If all buttons are in the right spot then we win!
        return true;
    }

    public static boolean swap(ButtonSquare[][] btns, ButtonSquare one, ButtonSquare two) {
        // Check if the two buttons are neighbors
        if (isNeighbor(one, two)) {
            // Set the holder variable values
            swapHolder.setFileName(two.getFileName());
            swapHolder.setOrder(two.getOrder());
            swapHolder.setImage(two.getBtn().getGraphic());
            swapHolder.getBtn().setId(two.getBtn().getId());

            // Swap the second with the first
            two.setImage(one.getImage());
            two.setOrder(one.getOrder());
            two.setFileName(one.getFileName());
            two.getBtn().setGraphic(one.getBtn().getGraphic());
            two.getBtn().setId(one.getBtn().getId());


            // Swap the first with the holder, the reason that we are doing this
            // Is so we don't need to generate new images in the memory space when buttons are swapped
            one.setImage(swapHolder.getImage());
            one.setOrder(swapHolder.getOrder());
            one.setFileName(swapHolder.getFileName());
            one.getBtn().setGraphic(swapHolder.getImage());
            one.getBtn().setId(swapHolder.getBtn().getId());
            return true;
        }
        return false;
    }
    /* Set's the preview image */
    public static void setPreview(String imageRepo, Button preview) {
        preview.setGraphic(new ImageView(new Image(logic.class.getResourceAsStream("Images/" + imageRepo + "_Thumbnail.png"))));
    }

    /* Blanks out a square by setting the preview image to a blank one */
    public static void setBlankSquare(ButtonSquare square) {
        square.getBtn().setGraphic(new ImageView(new Image(logic.class.getResourceAsStream("Images/BLANK.png"))));
    }

    /*
        Generates a random board given the imageRepository and the values where they should be assigned to
     */
    public static void setRandomBoard(String imageRepo, ButtonSquare[][] btns) {
        // Generate a temporary list of holder values
        String[]   randomNums = new String[16];
        int        pos;
        int        c = 0;
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                randomNums[c] = x + "" + y;
                c++;
            }
        }

        // Pick an image for each of the
        int numLeft = 16;
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                pos = (int) (Math.random() * numLeft);
                // btns[x][y].setOrder(Integer.parseInt(randomNums[pos]));
                btns[x][y].setFileName("Images/" + imageRepo + "_" + randomNums[pos] + ".png");
                btns[x][y].getBtn().setGraphic(new ImageView(new Image(logic.class.getResourceAsStream(btns[x][y].getFileName()))));
                randomNums[pos] = randomNums[numLeft - 1];
                numLeft--;
            }
        }
        setBlankSquare(btns[0][0]);
    }

    /* Clears the board by setting all the orders to 0 and also setting all their graphics to blank */
    public static void setBlankBoard(ButtonSquare[][] btns) {
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                btns[x][y].setOrder(0);
                btns[x][y].setFileName("Images/BLANK.png");
                // Since this is a static method, java has issues with getClass()
                btns[x][y].getBtn().setGraphic(new ImageView(new Image(logic.class.getResourceAsStream(btns[x][y].getFileName()))));
            }
        }
    }
}
